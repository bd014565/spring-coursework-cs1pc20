#include "floatlist.hpp"
// floatList.cpp
#include <iostream>

void floatList::appendNode(float num) {
    ListNode* newNode = new ListNode; // allocate memory for new node
    newNode->value = num; // assign value to new node
    newNode->next = nullptr; // make new node point to null
    if (head == nullptr) { // if the list is empty, make the new node the head
        head = newNode;
    }
    else { // if the list is not empty, traverse to the end and append the new node
        ListNode* currNode = head;
        while (currNode->next != nullptr) {
            currNode = currNode->next;
        }
        currNode->next = newNode;
    }
}

void floatList::displayList() {
    if (head == nullptr) { // if the list is empty, print an error message
        std::cout << "List is empty.\n";
    }
    else { // if the list is not empty, traverse and print each node's value
        ListNode* currNode = head;
        while (currNode != nullptr) {
            std::cout << currNode->value << " ";
            currNode = currNode->next;
        }
        std::cout << std::endl;
    }
}

void floatList::deleteNode(float num) {
    if (head == nullptr) { // if the list is empty, print an error message
        std::cout << "List is empty. Cannot delete node.\n";
    }
    else if (head->value == num) { // if the value to be deleted is at the head of the list
        ListNode* tempNode = head; // temporarily store the head node
        head = head->next; // make the next node the new head
        delete tempNode; // deallocate memory for the original head node
        std::cout << num << " is deleted\n";
    }
    else { // if the value to be deleted is not at the head of the list, traverse and delete the node
        ListNode* currNode = head;
        ListNode* prevNode = nullptr;
        bool found = false;
        while (currNode != nullptr && !found) {
            if (currNode->value == num) { // if the value is found
                found = true;
                prevNode->next = currNode->next; // make the previous node point to the next node
                delete currNode; // deallocate memory for the node to be deleted
                std::cout << num << " is deleted\n";
            }
            else { // if the value is not found yet, continue traversing
                prevNode = currNode;
                currNode = currNode->next;
            }
        }
        if (!found) { // if the value is not found at all, print an error message
            std::cout << num << " not found in list. Cannot delete node.\n";
        }
    }
}

